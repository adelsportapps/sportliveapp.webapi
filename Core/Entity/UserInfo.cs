﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public class UserInfo : BaseHorodatage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        public UserType UserType { get; set; }
        public Sport Sport { get; set; }
        public ICollection<ClubEntry> ClubEntries { get; set; }
        public ICollection<Conversation> Conversations { get; set; }
    }
}
