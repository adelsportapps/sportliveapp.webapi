﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public class UserType : BaseHorodatage
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public UserRole UserRole { get; set; }
    }
}
