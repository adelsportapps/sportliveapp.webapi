﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public class Conversation : BaseHorodatage
    {
        public int Id { get; set; }
        public int UserInfoId { get; set; }
        public ICollection<Message> Messages { get; set; }
        
        public UserInfo UserInfo { get; set; }
    }
}
