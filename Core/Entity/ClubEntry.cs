﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public class ClubEntry : BaseHorodatage
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public int UserInfoId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Position { get; set; }

        public Club Club { get; set; }
        public UserInfo UserInfo { get; set; }
    }
}
