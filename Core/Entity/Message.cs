﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public class Message
    {
        public int Id { get; set; }
        public int UserInfoId { get; set; }
        public int ConversationId { get; set; }
        public string Body { get; set; }
        
        public UserInfo UserInfo { get; set; }
        public Conversation Conversation { get; set; }
    }
}
